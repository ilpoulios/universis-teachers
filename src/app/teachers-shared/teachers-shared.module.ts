import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit, Optional, SkipSelf, ModuleWithProviders} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {AppSidebarService} from './services/app-sidebar.service';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgSpinKitModule } from 'ng-spin-kit';
import {ModalModule} from 'ngx-bootstrap';
import {RouterModule} from '@angular/router';
import {LangComponent} from './lang-switch.component';
import {environment} from '../../environments/environment';
import {ApplicationSettingsConfiguration, SharedModule} from '@universis/common';

export declare interface ApplicationSettings extends ApplicationSettingsConfiguration {
  contactUrl: any;
  useDigitalSignature: boolean;
  navigationLinks?: any;
  websocket: string;
  checkTokenInfo: boolean;
  title?: string;
  header?: Array<any>;
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    BsDropdownModule,
    ModalModule,
    NgSpinKitModule,
    RouterModule,
    SharedModule
  ],
  declarations: [
    LangComponent
  ],
  exports: [
    LangComponent,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class TeachersSharedModule implements OnInit {
constructor(@Optional() @SkipSelf() parentModule: TeachersSharedModule,
              private _translateService: TranslateService) {
    if (parentModule) {
      // throw new Error(
      //    'TeachersSharedModule is already loaded. Import it in the AppModule only');
    }
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading students shared module');
      console.error(err);
    });
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TeachersSharedModule,
      providers: [
        AppSidebarService
      ]
    };
  }

  async ngOnInit() {
    // create promises chain
    const sources = environment.languages.map(async (language) => {
      const translations = await import(`../../assets/i18n/${language}.json`);
      this._translateService.setTranslation(language, translations, true);
    });
    // execute chain
    await Promise.all(sources);
  }
}
