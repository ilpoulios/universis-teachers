import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {HttpClient} from '@angular/common/http';
import {ConfigurationService} from '@universis/common';


@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(private _context: AngularDataContext,
              private _http: HttpClient,
              private _configuration: ConfigurationService) { }

  sendMessageToStudent(courseClassId, studentId, responseModel, actionId= null) {

    const formData: FormData = new FormData();
    if (responseModel.subject) {
      formData.append('subject', responseModel.subject);
    }
    if (actionId) {
      formData.append('action', actionId);
    }
    formData.append('body', responseModel.body);

    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`/instructors/me/classes/${courseClassId}/students/${studentId}/sendMessage`);

    return this._http.post(postUrl, formData, {
      headers: serviceHeaders
    });
  }
  sendMessageToClass(courseClassId, responseModel, actionId= null) {

    const formData: FormData = new FormData();
    if (responseModel.file) {
      formData.append('file', responseModel.file, responseModel.file.name);
    }
    if (responseModel.subject) {
      formData.append('subject', responseModel.subject);
    }
    if (actionId) {
      formData.append('action', actionId);
    }
    formData.append('body', responseModel.body);


    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`/instructors/me/classes/${courseClassId}/students/sendMessage`);

    return this._http.post(postUrl, formData, {
      headers: serviceHeaders
    });
  }

  /**
   *
   * Creates a message for courseExamParticipants
   *
   * @param {number} courseExamId The id of the course exam
   * @param {any} responseModel The form data of the message model
   * @param {number} actionId
   *
   */
  sendMessageToExamParticipants(courseExamId: number, responseModel: any, actionId: number = null): Promise<any> {
    const formData: FormData = new FormData();
    if (responseModel.file) {
      formData.append('file', responseModel.file, responseModel.file.name);
    }
    if (responseModel.subject) {
      formData.append('subject', responseModel.subject);
    }
    if (actionId) {
      formData.append('action', actionId.toString());
    }
    formData.append('body', responseModel.body);
    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`/instructors/me/exams/${courseExamId}/students/sendMessage`);

    return this._http.post(postUrl, formData, {
      headers: serviceHeaders
    }).toPromise();
  }

  getAllMessages(take, skip): any {
    return this._context.model('instructors/me/messages')
      .asQueryable()
      .expand('recipient, action, attachments, sender')
      .orderByDescending('dateCreated')
      .skip(skip)
      .take(take)
      .getList();
  }

  getUnreadMessages(take, skip): any {
    return this._context.model('instructors/me/messages')
      .asQueryable()
      .expand('recipient, action, attachments, sender')
      .orderByDescending('dateCreated')
      .where('dateReceived').equal(null)
      .take(take)
      .skip(skip)
      .getList();
  }

  getMessagesByDate(take, skip, currentDate: string) {
    return this._context.model('instructors/me/messages')
      .asQueryable()
      .expand('recipient, action, attachments, sender')
      .orderByDescending('dateCreated')
      .where('dateCreated').greaterOrEqual(currentDate)
      .take(take)
      .skip(skip)
      .getList();
  }

  searchMessages(searchText: string, skip, take) {
    return this._context.model(`instructors/me/messages`)
      .asQueryable().expand('recipient, action, attachments, sender')
      .where('subject').contains(searchText)
      .or('action/additionalType').contains(searchText)
      .or('body').contains(searchText)
      .orderByDescending('dateCreated')
      .skip(skip)
      .take(take)
      .getList();
  }

  getCountOfUnreadMessages(): any {
    return this._context.model('instructors/me/messages')
      .asQueryable()
      .select('count(id) as total')
      .where('dateReceived').equal(null)
      .getItems();
  }

  setMessageAsRead(messageId) {
    return this._context.model(`instructors/me/messages/${messageId}/markAsRead`).save(null);
  }

  downloadFile(attachment) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');

    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        let downloadName = `${attachment.name}`
        a.download = downloadName;
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(blob, downloadName );
        } else {
          a.click();
        }
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }
}
