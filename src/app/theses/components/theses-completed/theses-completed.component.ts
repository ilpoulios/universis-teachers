import {Component, HostListener, OnInit} from '@angular/core';
import {ThesesService} from '../../services/theses.service';
import {NavigationEnd, Router} from '@angular/router';
import {LoadingService} from '@universis/common';
import {ErrorService} from '@universis/common';

@Component({
  selector: 'app-theses-completed',
  templateUrl: './theses-completed.component.html',
  styleUrls: ['./theses-completed.component.scss']
})

export class ThesesCompletedComponent implements OnInit {
  public completedTheses: any = [];
  public isLoading = true;
  public selectedIndex = 0;
  public isSrolling = true;

  constructor( private thesesServices: ThesesService,
               private router: Router,
               private _loadingService: LoadingService,
               private _errorService: ErrorService
             ) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const tree = router.parseUrl(router.url);
        if (tree.fragment) {
          const element = document.querySelector('#' + tree.fragment);
          if (element) {
            element.scrollIntoView();
          }
        }
      }
    });
  }

  ngOnInit() {
    this._loadingService.showLoading();
    this.thesesServices.getCompletedTheses().then((res) => {
      this.completedTheses = res;
      this.isLoading = false;
      this._loadingService.hideLoading();
    }).catch(err => {
      // hide loading
      this._loadingService.hideLoading();
      return this._errorService.navigateToError(err);
    });
  }

  setIndex(index: number) {
    this.selectedIndex = index;
    this.isSrolling = false;
  }

  @HostListener('scroll', ['$event'])
  scrollHandler(event) {
    return this.getScroll();
  }

  public getScroll( node = document.getElementById('inner__content') ): number {

    if ( !this.isSrolling ) {
      this.isSrolling = true;
      return null;
    }

    const currentScroll = node.scrollTop;
    const elements = document.getElementsByClassName('years');
    let fountActive = false;

    this.selectedIndex = -1;

    for (let i = 0; i < elements.length; i++) {
      const currentElement = elements.item(i);

      let currentOffSetTop = currentElement.getBoundingClientRect().top;
      if (currentOffSetTop < 0) {
        currentOffSetTop *= -1;
      }

      if (((currentElement.getBoundingClientRect().top > 0) || (currentElement.getBoundingClientRect().height - 60) >
        (node.getBoundingClientRect().top + currentOffSetTop)) && !fountActive) {

        this.selectedIndex = i;
        fountActive = true;
      }
    }
    return( currentScroll );
  }

}

