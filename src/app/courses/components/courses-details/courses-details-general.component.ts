import { Component, OnInit, Input } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {BsDropdownModule} from 'ngx-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {ErrorService} from '@universis/common';
import {LoadingService} from '@universis/common';
import {template, at} from 'lodash';
import {ProfileService} from '../../../profile/services/profile.service';


@Component({
  selector: 'app-courses-details-general',
  templateUrl: './courses-details-general.component.html',
  styleUrls: ['./courses-details.component.scss']
})
export class CoursesDetailsGeneralComponent implements OnInit {
  public selectedClass: any;
  public isLoading = true;        // Only if data is loaded

  constructor(private _context: AngularDataContext,
              private translate: TranslateService,
              private coursesService: CoursesService,
              private errorService: ErrorService,
              private loadingService: LoadingService,
              private router: Router,
              private route: ActivatedRoute,
              private _profileService: ProfileService) {
  }

  ngOnInit() {

    // show loading
    this.loadingService.showLoading();
    this.route.parent.params.subscribe(routeParams => {
      this.coursesService.getCourseClass(routeParams.course, routeParams.year, routeParams.period).then(courseClass => {
        // set selected course class
        this.selectedClass = courseClass;
        // get class and elearning url from instructor/department/organization/instituteConfiguration
        this._profileService.getInstructor().then(instructor => {
          if (instructor && instructor.department && instructor.department.organization &&
            instructor.department.organization.instituteConfiguration) {
            const instituteConfig = instructor.department.organization.instituteConfiguration;
            this.selectedClass.classUrl = instituteConfig.courseClassUrlTemplate ?
                template(instituteConfig.courseClassUrlTemplate)(this.selectedClass) : null;
            this.selectedClass.eLearningUrl = instituteConfig.eLearningUrlTemplate ?
                template(instituteConfig.eLearningUrlTemplate)(this.selectedClass) : null;
          }

          // hide loading
          this.loadingService.hideLoading();
          this.isLoading = false;
        });
      }).catch(err => {
        // hide loading
        this.loadingService.hideLoading();
        return this.errorService.navigateToError(err);
      });
    });
  }

  public openUrl(url) {
    if (url) {
      window.open(url, '_blank');
    }
  }

}
