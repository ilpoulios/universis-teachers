import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesRecentComponent } from './courses-recent.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { MostModule } from '@themost/angular';
import { ProfileService } from 'src/app/profile/services/profile.service';
import { ErrorService } from '@universis/common';
import { ClassesService } from 'src/app/classes/services/classes.service';
import { LoadingService } from '@universis/common';
import { CoursesService } from '../../services/courses.service';
import { BsModalService, ModalModule, ComponentLoaderFactory, PositioningService, TooltipModule } from 'ngx-bootstrap';
import { SharedModule } from '@universis/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CoursesRecentComponent', () => {
  let component: CoursesRecentComponent;
  let fixture: ComponentFixture<CoursesRecentComponent>;

  const profileSvc = jasmine.createSpyObj('ProfleService,', ['getInstructor']);
  const errorSvc = jasmine.createSpyObj('ErrorService,', ['getLastError', 'navigateToError', 'setLastError']);
  const classesSvc = jasmine.createSpyObj('ClassesService,', ['getAllInstructors', 'getAllClasses', 'getCurrentClasses']);
  const coursesSvc = jasmine.createSpyObj('CoursesService,', ['getAllInstructors', 'getCourseExams', 'getCourseExam', 'getCourseClassExams',
                                          'getCourseExams2', 'getRecentCourses', 'getCourseCurrentExams', 'getAllClasses', 'getCourseClass',
                                          'getCourseClassStudents', 'searchCourseClassStudents', 'getCourseClassList', 'getStudentList',
                                          'getStudentBySearch', 'uploadGradesFile', 'getCurrentCourseExams', 'setUploadToComplete',
                                          'getUploadHistory', 'getUploadHistory']);


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        SharedModule,
        TranslateModule.forRoot(),
        RouterTestingModule,
        TooltipModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
              useMediaTypeExtensions: false
          }
        })
      ],
      declarations: [ CoursesRecentComponent ],
      providers:[
        {
          provide: ProfileService,
          useValue: profileSvc
        },
        {
          provide: ErrorService,
          useValue: errorSvc
        },
        {
          provide: ClassesService,
          useValue: classesSvc
        },
        {
          provide: CoursesService,
          useValue: coursesSvc
        },
        LoadingService,
        BsModalService,
        ComponentLoaderFactory,
        PositioningService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesRecentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
